
import { useEffect, useState, useContext } from 'react';
import { NextPage,GetStaticPaths, GetStaticProps, GetServerSidePropsContext, GetServerSideProps} from 'next';
import {Pokemon} from  '../../interfaces/poke';


interface Props {
 poke:Pokemon
}

const Dinamic:NextPage<Props>=({ 

poke

}) => {

  

  return (

    <>
    
    <h1> el nombre del pokemon es: {poke.name} </h1>
    
    
    </>

  );

}



export default Dinamic;




export const getServerSideProps: GetServerSideProps = async({query: {id}})=>{

  const url = await fetch(`https://pokeapi.co/api/v2/pokemon/${id}`)

  console.log ('url de prueba ',url);
  const data: Pokemon  = await url.json()
  console.log ('data de prueba ',data)

  return {

    props:{

        poke: data
      

    }

  }

}
