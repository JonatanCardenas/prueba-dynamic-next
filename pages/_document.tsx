import React from 'react';
import Document, { Head, Html, Main, NextScript } from 'next/document';
import type { DocumentContext } from 'next/document';


const MyDocument = () => (

  <Html lang="en">
    <Head>
      <link rel='shorcut icon' href='/favicon/favicon.ico' />
    </Head>
    <body className='content-body'>
      <Main />
      <NextScript />
    </body>
  </Html>

);

MyDocument.getInitialProps = async (ctx: DocumentContext) => {
  const initialProps = await Document.getInitialProps(ctx);
  return {
    ...initialProps,
    styles: (
      <>
        {initialProps.styles}
      </>
    ),
  };
}


export default MyDocument;